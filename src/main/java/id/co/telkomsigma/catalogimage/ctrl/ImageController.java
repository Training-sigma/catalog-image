package id.co.telkomsigma.catalogimage.ctrl;


import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class ImageController {

	   @GetMapping("/get-text")
	    public @ResponseBody String getText() {
	        return "Hello world";
	    }

	    @GetMapping("/get-image")
	    public @ResponseBody byte[] getImage() throws IOException {
	        final InputStream in = getClass().getResourceAsStream("/assest/image/java.png");
	        return IOUtils.toByteArray(in);
	    }

	    @GetMapping(value = "/get-image-with-media-type", produces = MediaType.IMAGE_JPEG_VALUE)
	    public @ResponseBody byte[] getImageWithMediaType() throws IOException {
	        final InputStream in = getClass().getResourceAsStream("/assest/image/java.png");
	        return IOUtils.toByteArray(in);
	    }

	    @GetMapping(value = "/get-file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	    public @ResponseBody byte[] getFile() throws IOException {
	        final InputStream in = getClass().getResourceAsStream("/assest/image/data.txt");
	        return IOUtils.toByteArray(in);
	    }
}
