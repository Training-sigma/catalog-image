package id.co.telkomsigma.catalogimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CatalogImageApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogImageApplication.class, args);
	}
}
